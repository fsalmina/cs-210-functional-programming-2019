% Implicits
%
%

Reminder: General `sort` Operation
==================================

~~~
def sort[A](xs: List[A])(ord: Ordering[A]): List[A] = ...
~~~

Problem: Passing around `Ordering` values is cumbersome.

~~~
sort(xs)(Ordering.Int)
sort(ys)(Ordering.Int)
sort(strings)(Ordering.String)
~~~

Sorting a `List[Int]` instance always uses the same `Ordering.Int` value,
sorting a `List[String]` instance always uses the same `Ordering.String`
value, and so on…

Implicit Parameters
===================

We can reduce the boilerplate by making `ord` an **implicit** parameter.

~~~
def sort[A](xs: List[A])(implicit ord: Ordering[A]): List[A] = ...
~~~

- A method can have only one implicit parameter list, and it must be the last
  parameter list given.

Then calls to `sort` can omit the `ord` parameter:

~~~
sort(xs)
sort(ys)
sort(strings)
~~~

The compiler infers the implicit parameter value based on the
**queried type**.

Implicit Parameters (2)
=======================

~~~
def sort[A](xs: List[A])(implicit ord: Ordering[A]): List[A] = ...

val xs: List[Int] = ...
~~~

->

~~~
sort(xs)
~~~

->

~~~
sort[Int](xs)
~~~

->

~~~
sort[Int](xs)(Ordering.Int)
~~~

In this case, the queried type is `Ordering[Int]`.

Implicit Parameters Resolution
==============================

Say, a function takes an implicit parameter of type `T`.

The compiler will search an implicit **definition** that:

- is marked `implicit`,
- has a type compatible with `T`,
- is visible at the point of the function call, or is defined
  in a companion object associated with `T`.

If there is a single (most specific) definition, it will be taken
as actual arguments for the implicit parameter.

Otherwise it’s an error.

Implicit Definitions
====================

For the previous example to work, the `Ordering.Int` value definition
must be marked `implicit`:

~~~
object Ordering {

  implicit val Int: Ordering[Int] = ...

}
~~~

Implicit Search
===============

The implicit search for a type `T` includes:

- all the implicit definitions that are visible (inherited, imported,
  or defined in an enclosing scope),
- the *implicit scope* of type `T`, made of implicit definitions found
  in a companion object *associated* with `T`. In essence$^*$, the types
  associated with a type `T` are:
    - if `T` is a compound type $T_1 with T_2 ... with T_n$, the union
      of the parts of $T_1$, ... $T_n$ as well as $T$ itself,
    - if `T` is a parameterized type $S[T_1, T_2, ..., T_n]$, the union
      of the parts of $S$ and $T_1$, ..., $T_n$,
    - otherwise, just `T` itself.

In the case of the `sort(xs)` call, the compiler looks for an implicit
`Ordering[Int]` definition, which is found in the `Ordering` companion
object.

Implicit Not Found
==================

If there is no available implicit definition matching the queried type,
an error is reported:

~~~
scala> def f(implicit n: Int) = ()
scala> f
       ^
error: could not find implicit value for parameter n: Int
~~~

Ambiguous Implicit Definitions
==============================

If more than one implicit definition are eligible, an **ambiguity** is reported:

~~~
scala> implicit val x: Int = 0
scala> implicit val y: Int = 1
scala> def f(implicit n: Int) = ()
scala> f
       ^
error: ambiguous implicit values:
 both value x of type => Int
 and value y of type => Int
 match expected type Int
~~~

Priorities
==========

Actually, several implicit definitions matching the same type don’t generate an
ambiguity if one is **more specific** than the other.

In essence$^{*}$, a definition `a: A` is more specific than a definition `b: B` if:

- type `A` is a subtype of type `B`,
- type `A` has more “fixed” parts,
- `a` is defined in a class or object which is a subclass of the class defining `b`.

Priorities: Example (1)
=======================

Which implicit definition matches the queried `Int` implicit parameter when
the `f` method is called?

~~~
implicit def universal[A]: A = ???
implicit def int: Int = ???

def f(implicit n: Int) = ()

f
~~~

Priorities: Example (2)
=======================

Which implicit definition matches the queried `Int` implicit parameter when
the `f` method is called?

~~~
trait A {
  implicit val x: Int = 0
}
trait B extends A {
  implicit val y: Int = 1
  
  def f(implicit n: Int) = ()
  
  f
}
~~~

Context Bounds
==============

A syntactic sugar allows the omission of the implicit parameter list:

~~~
def printSorted[A: Ordering](as: List[A]): Unit = {
  println(sort(as))
}
~~~

Type parameter `A` has one **context bound**: `Ordering`. There must be an
implicit value with type `Ordering[A]` at the point of application.

More generally, a method definition such as:

$def f[A: U_1 ... : U_n](ps): R = ...$

Is expanded to:

$def f[A](ps)(implicit ev_1: U_1[A], ..., ev_n: U_n[A]): R = ...$

Implicit Query
==============

At any point in a program, one can **query** an implicit value of
a given type by calling the `implicitly` operation:

~~~
scala> implicitly[Ordering[Int]]
res0: Ordering[Int] = scala.math.Ordering$Int$@73564ab0
~~~

`implicitly` is not a special keyword, it is defined as a library operation:

~~~
def implicitly[A](implicit value: A): A = value
~~~

Summary
=======

In this lecture we have introduced the concept of **implicit programming**,
a language mechanism that infers **values** by using **type** information.

There has to be a **unique** implicit definition matching the queried type
for it to be used by the compiler.

Implicit values are searched in the enclosing **lexical scope** (imports,
parameters, inherited members) as well as in the **implicit scope** made
of implicits defined in companion objects of types associated with the
queried type.

