% Implicit Conversions
%
%

Implicit Conversions
====================

The last implicit-related mechanism of the language is implicit
**conversions**.

They make it possible to convert an expression to a different
type.

This mechanism is usually used to provide more ergonomic APIs:

~~~
// { "name": "Paul", "age": 42 }
Json.obj("name" -> "Paul", "age" -> 42)


val delay = 15.seconds
~~~

Type Coercion: Motivation (1)
=============================

~~~
sealed trait Json
case class JNumber(value: BigDecimal) extends Json
case class JString(value: String) extends Json
case class JBoolean(value: Boolean) extends Json
case class JArray(elems: List[Json]) extends Json
case class JObject(fields: (String, Json)*) extends Json
~~~

->

~~~
// { "name": "Paul", "age": 42 }
JObject("name" -> JString("Paul"), "age" -> JNumber(42))
~~~

Problem: Constructing JSON objects is too verbose.

Type Coercion: Motivation (2)
=============================

~~~
sealed trait Json
case class JNumber(value: BigDecimal) extends Json
case class JString(value: String) extends Json
case class JBoolean(value: Boolean) extends Json
case class JArray(elems: List[Json]) extends Json
case class JObject(fields: (String, Json)*) extends Json
~~~

~~~
// { "name": "Paul", "age": 42 }
Json.obj("name" -> "Paul", "age" -> 42)
~~~

How could we support the above user-facing syntax?

Type Coercion: Motivation (3)
=============================

~~~
// { "name": "Paul", "age": 42 }
Json.obj("name" -> "Paul", "age" -> 42)
~~~

What could be the type signature of the `obj` constructor?

->

~~~
def obj(fields: (String, Any)*): Json
~~~

->

Allows invalid JSON objects to be constructed!

~~~
Json.obj("name" -> ((x: Int) => x + 1))
~~~

We want invalid code to be signaled to the programmer with a
compilation error.

Type Coercion (1)
=================

~~~
object Json {

  def obj(fields: (String, JsonField)*): Json =
    JObject(fields.map(_.toJson): _*)
    
  trait JsonField {
    def toJson: Json
  }

}
~~~

Type Coercion (2)
=================

~~~  
trait JsonField {
  def toJson: Json
}

object JsonField {
  implicit def stringToJsonField(s: String): JsonField = () => JString(s)
  implicit def intToJsonField(n: Int): JsonField = () => JNumber(n)
  ...
  implicit def jsonToJsonField(j: Json): JsonField = () => j
}
~~~

Type Coercion: Usage
====================

~~~
Json.obj("name" -> "Paul", "age" -> 42)
~~~

->

The compiler implicitly inserts the following conversions:

~~~
Json.obj(
  "name" -> Json.JsonField.stringToJsonField("Paul"),
  "age" -> Json.JsonField.intToJsonField(42)
)
~~~

Extension Methods: Motivation (1)
=================================

~~~
case class Duration(value: Int, unit: TimeUnit)
~~~

->

~~~
val delay = Duration(15, TimeUnit.Second)
~~~

Extension Methods: Motivation (2)
=================================

~~~
case class Duration(value: Int, unit: TimeUnit)
~~~

~~~
val delay = 15.seconds
~~~

How could we support the above user-facing syntax?

Extension Methods
=================

~~~
case class Duration(value: Int, unit: TimeUnit)

object Duration {

  object Syntax {
    implicit def hasSeconds(n: Int): HasSeconds = new HasSeconds(n)
  }
  
  class HasSeconds(n: Int) {
    def seconds: Duration = Duration(n, TimeUnit.Second)
  }

}
~~~

Extension Methods: Usage
========================

~~~
import Duration.Syntax._

val delay = 15.seconds
~~~

->

The compiler implicitly inserts the following conversion:

~~~
val delay = hasSeconds(15).seconds
~~~

Implicit Conversions
====================

The compiler looks for implicit conversions on an expression `e` of type `T`
in the following situations:

- `T` does not conform to the expression’s expected type,
- in a selection `e.m`, if member `m` is not accessible on `T`,
- in a selection `e.m(args)`, if member `m` is accessible on `T` but is not
  applicable to the arguments `args`.

Note: at most one implicit conversion can be applied to a given expression.

Summary
=======

- Implicit conversions can improve the ergonomics of an API
