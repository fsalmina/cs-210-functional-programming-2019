#!/bin/sh
SCRIPT="$(cd "${0%/*}" 2>/dev/null; echo "$PWD"/"${0##*/}")"
DIR=`dirname "${SCRIPT}"}`
exec scala -save $0 $DIR $SCRIPT $@
::!#

import scala.sys.process._
import scala.io.Source
import java.io.{FileWriter, PrintWriter, File, FileInputStream, FileOutputStream}

object App {

  case class Slide(title: String, body: List[String]) {
    override val toString =
      title + "\n" + body.mkString("\n")
  }

  val equalsSignTitlePat = "^=+$".r
  val hashSignTitlePat = "^# .*$".r
  val arrowPat = "^->.*$".r

  val isNotTitle: ((String, String)) => Boolean = {
    case (currLine, nextLine) =>
      equalsSignTitlePat.findFirstIn(nextLine).isEmpty && hashSignTitlePat.findFirstIn(currLine).isEmpty
  }

  def mkSlides(lines: List[String]): List[Slide] = {
    if (lines.isEmpty) List()
    else {
      // the first line is the title of the first slide
      // find out whether we have to strip leading #
      val (title, rest) =
        if (lines(0).startsWith("#")) (lines(0), lines.tail)
        else (lines(0) + "\n" + lines(1), lines drop 2)

      val (body, rest1) = partitionOnTitle(rest)

      Slide(title, body) :: mkSlides(rest1)
    }
  }

  def partitionOnTitle(lines: List[String]): (List[String], List[String]) = {
    val linePairs = lines.init zip lines.tail
    // fromTitle might be an empty list
    val (beforeTitle, fromTitle) = (linePairs takeWhile isNotTitle, linePairs dropWhile isNotTitle)
    if (fromTitle.isEmpty)
      (lines, List())
    else
      (beforeTitle.map(_._1), fromTitle.map(_._1) :+ fromTitle.last._2)
  }

  def expandIncrementalSlides(slides: List[Slide]): List[Slide] =
    slides flatMap expand

  def hasArrow(slide: Slide) =
    slide.body exists matchesArrow

  def matchesArrow(line: String) =
    !arrowPat.findFirstIn(line).isEmpty

  def expand(slide: Slide): List[Slide] =
    if (!hasArrow(slide)) List(slide)
    else {
      val linesBeforeFirstArrow =
        slide.body.takeWhile(line => !matchesArrow(line))

      val firstSlide = Slide(slide.title, linesBeforeFirstArrow)

      // now create a modified slide which removes the first arrow
      val slideWithoutFirstArrow = {
        val linesStartingWithArrow = slide.body.dropWhile(line => !matchesArrow(line))

        val (modifiedLine, unchangedRest) =
          (if (linesStartingWithArrow(0).length > 2) linesStartingWithArrow(0).substring(3)
           else linesStartingWithArrow(0).substring(2), linesStartingWithArrow.tail)

        Slide("\n" + slide.title, linesBeforeFirstArrow ::: List(modifiedLine) ::: unchangedRest)
      }

      firstSlide :: expand(slideWithoutFirstArrow)
    }

  def mdPreprocess(src: String, wdir: File): Unit = {
    val sourceFile = Source.fromFile(new File(src+".md")).getLines.toList

    val outFile = new File(wdir, src+".md")
    val fileWriter = new PrintWriter(new FileWriter(outFile))

    val (frontMatter, rawSlides) = partitionOnTitle(sourceFile)
    frontMatter foreach fileWriter.println

    val slides = mkSlides(rawSlides)
    expandIncrementalSlides(slides) foreach fileWriter.println

    fileWriter.flush()
    fileWriter.close()
  }

  def latexPreprocess(src: String, wdir: File): Unit = {
    val sourceFile = Source.fromFile(new File(wdir, src+".tex")).getLines

    // preprocessing for beamer
    val sectionPat = "\\\\section".r
    val noSectionTags = for (line <- sourceFile) yield {
      sectionPat replaceAllIn(line, "\\\\end{frame}\n\\\\begin{frame}[fragile]\\\\frametitle") //\\\\emph{}
    }

    val endPat = "\\\\end\\{document\\}".r
    val properEndDocument = for (line <- noSectionTags) yield {
      endPat replaceAllIn(line, "\\\\end{frame}\n\\\\end{document}")
    }

    // preprocessing for pygments automatic syntax highlighting
    val begVerbatimPat = "\\\\begin\\{verbatim\\}".r
    val noBeginVerbatim = for (line <- properEndDocument) yield {
      begVerbatimPat replaceAllIn (line, "\\\\begin{minted}{scala}")
    }

    val endVerbatimPat = "\\\\end\\{verbatim\\}".r
    val noEndVerbatim = for (line <- noBeginVerbatim) yield {
      endVerbatimPat replaceAllIn (line, "\\\\end{minted}")
    }

    val out = noEndVerbatim

    val fileWriter = new PrintWriter(new FileWriter(new File(wdir, src+"-out.tex")))
    out foreach fileWriter.println
    fileWriter.flush()
    fileWriter.close()
  }

  def copyDir(srcDir: File, destDir: File): Unit = {
    if (srcDir.isDirectory()) {
      if (!destDir.exists) destDir.mkdir()

      val children = srcDir.list()
      for (child <- children) copyDir(new File(srcDir, child), new File(destDir, child))

    } else copyFile(srcDir, destDir)
  }

  def copyFile(src: File, dest: File): Unit = {
    val in = new FileInputStream(src)
    val out = new FileOutputStream(dest)

    val buf = Array.ofDim[Byte](1024)
    var len = in.read(buf)
    while (len > 0) {
      out.write(buf, 0, len)
      len = in.read(buf)
    }
    in.close()
    out.close()
  }

  def args3plus(args: Array[String]): String = {
    val al = args.length
    if (al <= 3) "" else {
      var res = args(3)
      var i= 4
      while (i < al) {
	res += " " + args(i)
	i += 1
      }
      res
    }
  }

  def main(args: Array[String]): Unit = {
    val sourceFile = args(2)
    val genDir = new File("gen/")
    val texDir = new File("gen/tex/")
    val mdDir = new File("gen/md/")

    if (!texDir.exists) texDir.mkdirs()
    if (!mdDir.exists) mdDir.mkdirs()

    val commonDir = new File("common/")
    val commonDirCopy = new File("gen/tex/common/")
    if (!commonDirCopy.exists) commonDirCopy.mkdir()
    copyDir(commonDir, commonDirCopy)

    val commonMdDirCopy = new File("gen/md/common/")
    if (!commonMdDirCopy.exists) commonMdDirCopy.mkdir()
    copyDir(commonDir, commonMdDirCopy)

    val imageDir = new File("images/")
    val imageDirCopy = new File("gen/tex/images")
    if (!imageDirCopy.exists) imageDirCopy.mkdir()
    copyDir(imageDir, imageDirCopy)

    val imageMdDirCopy = new File("gen/md/images")
    if (!imageMdDirCopy.exists) imageMdDirCopy.mkdir()
    copyDir(imageDir, imageMdDirCopy)

    mdPreprocess(sourceFile, mdDir)

    println("Invoking pandoc")
    val pandocArgs = args3plus(args)
    val pandoc = "pandoc " + pandocArgs + " --template common/beamer.template gen/md/"+args(2)+".md -o gen/tex/"+args(2)+".tex"
    pandoc.!
    latexPreprocess(sourceFile, texDir)

    val outTex = new File(texDir, sourceFile+"-out.tex")
    val tex = new File(texDir, sourceFile+".tex")
    outTex.renameTo(tex)
    outTex.delete()

    println("Invoking latex")
    val pdflatex = "xelatex --shell-escape "+ sourceFile +".tex"
    Process(pdflatex, texDir).!

    val texPdf = new File(texDir, sourceFile+".pdf")
    val genPdf = new File(genDir, sourceFile+".pdf")
    texPdf.renameTo(genPdf)
    texPdf.delete()

    println("Invoking pdfnup")
    val scaledPdf = new File(genDir, sourceFile + "-scaled.pdf")
    val pdfnup = "pdfnup --papersize {33.75cm,60cm} --landscape --keepinfo --nup 1x1 --frame false --outfile " + scaledPdf + " " + genPdf
    Process(pdfnup).!
  }
}
