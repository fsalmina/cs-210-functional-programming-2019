% Type Classes vs Inheritance
%
%

Type Classes
============

In the previous lectures we have seen a particular pattern of code
to achieve *ad hoc* polymorphism:

~~~
trait Ordering[A] { def lt(a1: A, a2: A): Boolean }

object Ordering {
  implicit val Int: Ordering[Int] = (x, y) => x < y
  implicit val String: Ordering[String] = (s, t) => (s compareTo t) < 0
}

def sort[A: Ordering](xs: List[A]): List[A] = ...
~~~

We say that `Ordering` is a **type class**.

Alternatively, Using *Subtyping Polymorphism*
=============================================

~~~
trait Ordered {
  def lt(other: Ordered): Boolean
}
~~~

~~~
def sort2(xs: List[Ordered]): List[Ordered] = {
  ...
  ... if x lt y then ...
  ...
}
~~~

How do these approaches compare?

Usage
=====

(Assuming that `Int <: Ordered`)

~~~
val sortedInts: List[Int] = sort2(ints)
~~~

->

~~~
                            ^^^^^^^^^^^
error: type mismatch;
 found   : List[Ordered]
 required: List[Int]
~~~

->

~~~
def sort2(xs: List[Ordered]): List[Ordered]
~~~

First Improvement
=================

~~~
def sort2[A <: Ordered](xs: List[A]): List[A] = // ... same implementation
~~~

->

~~~
val sortedInts:   List[Int]    = sort2(ints)    // OK
val sortedString: List[String] = sort2(strings) // OK
~~~

... assuming `Int <: Ordered` and `String <: Ordered`!

Subtyping Polymorphism Causes Strong Coupling
=============================================

Subtyping polymorphism imposes a strong coupling between *operations*
(`Ordered`) and *data types* (`Int`, `String`) that support them.

By contrast, type classes encourage separating the definition of data types
and the operations they support: the `Ordering[Int]` and `Ordering[String]`
instances don’t have to be defined in the same unit as the `Int` and `String`
data types.

Type classes support *retroactive* extension: the ability to extend a data
type with new operations without changing the original definition of the data type.

Implementing an Operation for a Custom Data Type
================================================

~~~
case class Rational(num: Int, denom: Int)
~~~

->

~~~
case class Rational(num: Int, denom: Int) extends Ordered {
  def lt(other: Ordered) = other match {
    case Rational(otherNum, otherDenom) => num * otherDenom < otherNum * denom
    case _ => ???
  }
}
~~~

->

We want to compare rational numbers with other rational numbers only!

Second Improvement: F-Bounded Polymorphism
==========================================

~~~
trait Ordered[This <: Ordered[This]] {
  def lt(other: This): Boolean
}

case class Rational(num: Int, denom: Int) extends Ordered[Rational] {
  def lt(other: Rational) = num * other.denom < other.num * denom
}

def sort2[A <: Ordered[A]](xs: List[A]): List[A] = {
  ...
  ... if x lt y then ...
  ...
}
~~~

Implementing an Operation for a Custom Data Type (2)
====================================================

~~~
case class Rational(num: Int, denom: Int)

object Rational {
  implicit val ordering: Ordering[Rational] =
    (x, y) => x.num * y.denom < y.num * x.denom
}

sort(Rational(1, 2), Rational(1, 3))
~~~

->

(The `Ordering[Rational]` instance definition could be in a different project)

Dispatch Time
=============

Another difference between subtyping polymorphism and type classes is **when**
the dispatch happens.

- With type classes, the implicit instance is resolved at **compilation time**,
- With subtyping polymorphism, the actual implementation of a method is resolved
  at **run time**.

Summary
=======

In this lecture we have seen that type classes support retroactive
extensibility.

The dispatch happens at compile time with type classes, whereas it
happens at run time with subtype polymorphism.
